package de.volkswagen.mediacontrol.media.radiopresets.logo;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class CountryCodeUpdater {
    @DexIgnore
    private final long DATA_VALID_TIME = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    @DexIgnore
    private final de.volkswagen.mediacontrol.media.radiopresets.logo.AndroidLocationManager androidLocationManager;
    @DexIgnore
    private final android.content.Context context;
    @DexIgnore
    private java.lang.String countryCode;
    @DexIgnore
    private final de.volkswagen.mediacontrol.media.radiopresets.logo.CountryCodeUpdateListener countryCodeUpdateListener;
    @DexIgnore
    private de.volkswagen.mediacontrol.events.CurrentPositionUpdateEvent lastPositionUpdateEvent;

    @DexIgnore
    public CountryCodeUpdater(android.content.Context context2, de.volkswagen.mediacontrol.media.radiopresets.logo.CountryCodeUpdateListener countryCodeUpdateListener2) {
        this.context = context2;
        this.androidLocationManager = de.volkswagen.mediacontrol.media.radiopresets.logo.AndroidLocationManager.getInstance(context2);
        this.countryCodeUpdateListener = countryCodeUpdateListener2;
        startUpdatingLocation();
    }

    @DexIgnore
    public java.lang.String getCurrentCountryCode() {
        synchronized (this) {
            if (this.lastPositionUpdateEvent != null) {
                if (isLocationDataValid(this.lastPositionUpdateEvent)) {
                    java.lang.String str = this.countryCode;
                    return str;
                }
            }
            startUpdatingLocation();
            return null;
        }
    }

    @DexReplace
    private java.lang.String getCountryCode(de.volkswagen.mediacontrol.events.CurrentPositionUpdateEvent currentPositionUpdateEvent) {
        return new de.volkswagen.mediacontrol.media.radiopresets.logo.parser.CountryCodesParser(this.context).getCountryCode(com.tanapruk.reversegeocode.GeocodeListBuilder.getCountryId(this.context, java.lang.Double.valueOf(currentPositionUpdateEvent.getLatitude()), java.lang.Double.valueOf(currentPositionUpdateEvent.getLongitude()))).getECC().toLowerCase();
    }

    @DexIgnore
    private boolean isLocationDataValid(de.volkswagen.mediacontrol.events.CurrentPositionUpdateEvent currentPositionUpdateEvent) {
        return currentPositionUpdateEvent != null && currentPositionUpdateEvent.getTimestamp() - android.os.SystemClock.elapsedRealtime() < this.DATA_VALID_TIME;
    }

    @DexIgnore
    private void startUpdatingLocation() {
        de.volkswagen.mediacontrol.mhservice.MhEventBus.registerSticky(this);
        this.androidLocationManager.startListening();
    }

    @DexIgnore
    private void stopUpdatingLocation() {
        de.volkswagen.mediacontrol.mhservice.MhEventBus.unregister(this);
        this.androidLocationManager.stopListening();
    }

    @DexIgnore
    public void onEvent(de.volkswagen.mediacontrol.events.CurrentPositionUpdateEvent currentPositionUpdateEvent) {
        if (!isLocationDataValid(currentPositionUpdateEvent)) {
            startUpdatingLocation();
            return;
        }
        stopUpdatingLocation();
        synchronized (this) {
            this.lastPositionUpdateEvent = currentPositionUpdateEvent;
            this.countryCode = getCountryCode(currentPositionUpdateEvent);
            this.countryCodeUpdateListener.onCountryCodeUpdate();
        }
    }
}