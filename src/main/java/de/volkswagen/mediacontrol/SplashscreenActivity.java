package de.volkswagen.mediacontrol;

import android.content.Context;

import com.comarch.technologies.mobile.mediahubservice.upnp.model.UpnpDevice;

import de.volkswagen.mediacontrol.common.application.RseApplication;
import de.volkswagen.mediacontrol.mhservice.connection.ServiceBeaconData;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SplashscreenActivity extends de.volkswagen.mediacontrol.RseMagicBaseActivity implements de.volkswagen.mediacontrol.SplashscreenView {

    @DexIgnore
    public de.volkswagen.mediacontrol.SplashscreenPresenter mPresenter;

    @DexReplace
    @Override // de.volkswagen.mediacontrol.SplashscreenView
    public void showDriverDistractionInfo() {
//        de.volkswagen.mediacontrol.common.logging.Log.d(TAG, "showDriverDistractionInfo");
//        runOnUiThread(new de.volkswagen.mediacontrol.SplashscreenActivity.AnonymousClass5());
        de.volkswagen.mediacontrol.SplashscreenActivity.this.dismissAllPopups();
        de.volkswagen.mediacontrol.SplashscreenActivity.this.mPresenter.driverDistractionInfoAccepted();
        de.volkswagen.mediacontrol.SplashscreenActivity.this.mPresenter.initExtfile();

    }

    @DexReplace
    @Override // de.volkswagen.mediacontrol.SplashscreenView
    public void showNoConnectionPopup() {
//        if (this.noConnectionPopupHelper.isDismissed()) {
//            runOnUiThread(new AnonymousClass8());
//        }
        de.volkswagen.mediacontrol.SplashscreenActivity.this.dismissAllPopups();
    }


    @DexIgnore
    @Override
    public void dismissAllPopups() { }

    @DexIgnore
    @Override
    public Context getContext() {
        return null;
    }

    @DexIgnore
    @Override
    public RseApplication getRseApplication() {
        return null;
    }

    @DexIgnore
    @Override
    public void setButtonsConfiguration(ButtonsConfiguration buttonsConfiguration) { }

    @DexIgnore
    @Override
    public void showAppNeedsUpdate() { }

    @DexIgnore
    @Override
    public void showConditionsOfUse() { }

    @DexIgnore
    @Override
    public void showConditionsOfUseFragment() { }

    @DexIgnore
    @Override
    public void showNoMediaControlLicense() { }

    @DexIgnore
    @Override
    public void showPrivacyPoliceInfo() { }

    @DexIgnore
    @Override
    public void showPrivacyStatementFragment() { }

    @DexIgnore
    @Override
    public void showQueryAnswerFalse() { }

    @DexIgnore
    @Override
    public void showQueryRequestCanceled() { }

    @DexIgnore
    @Override
    public void showQueryRequestFailed() { }

    @DexIgnore
    @Override
    public void showResidenceCountryList() { }

    @DexIgnore
    @Override
    public void showTermsAndConditionsInfo() { }

    @DexIgnore
    @Override
    public void showTutorial() { }

    @DexIgnore
    @Override
    public void showWrongVendor() { }

    @DexIgnore
    @Override
    public void onBeacon(ServiceBeaconData serviceBeaconData) { }

    @DexIgnore
    @Override
    public void onWifiStateChanged(boolean b) { }

    @DexIgnore
    @Override
    public void onQueryDeviceAvailable(UpnpDevice upnpDevice) { }
}
