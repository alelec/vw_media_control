# java -jar ../apktool_2.4.0.jar d ../apk/Navdy-post-mortem-1.apk
# python3 ./strip.py
# java -jar ../apktool_2.4.0.jar b -o ./Navdy-post-mortem-1.apk Navdy-post-mortem-1


import sys
import os, re
import shutil
import fileinput
from pathlib import Path

for root, dirs, files in os.walk("./de.volkswagen.mediacontrol-1ac961c33f3027bd5aef8a1f54697a74"):
    remove = []
    for d in dirs:
        dirpath = str(Path(root) / d).replace('\\', '/')
        if dirpath.endswith('com/google/android/gms'):
            remove.append(d)
            path = os.path.join(root, d)
            print("deleting", path)
            shutil.rmtree(path)

        if len(Path(root).parts) == 3 and Path(root).parts[2] == 'android':
            if d in ('support',):
                remove.append(d)
                path = os.path.join(root, d)
                print("deleting", path)
                shutil.rmtree(path)
    for d in remove:
        dirs.remove(d)

    # for name in files:

    #     fpath = os.path.join(root, name)
    #     fname = origname = Path(fpath)

    #     if 'com/navdy/' not in fpath:
    #         # print('-', f)
    #         continue

    #     if not fpath.endswith('.smali'):
    #         # print('-', f)
    #         continue
       
    #     print(fname.name)

    #     if fpath.endswith('com/navdy/obd/Pids.smali'):
    #         fname = fname.rename(fname.parent / "PidsOrig.smali")

    #         text = target.read_text()
    #         text = text.replace("obd/Pids;", "obd/PidsOrig;")
    #         text = text.replace('"Pids.java"', '"PidsOrig.java"')
    #         target.write_text(text)
    #         continue

    #     if re.search(r'\$\d', fname.name):
    #         target = fname.with_name(fname.name.replace('$', '$Anon'))
            
    #         print(fname, "->", target)
    #         fname.rename(target)
            
    #     else:
    #         target = fname

    #     text = target.read_text()

    #     def add_anon(match):
    #         return "$Anon" + match.group(1)

    #     text = re.sub(r'\$(\d+)', add_anon, text)

        # target.write_text(text)
