# java -jar apktool_2.4.0.jar d ../apk/Navdy-post-mortem-1.apk
# python3 ./3080_dupes.py
# java -jar apktool_2.4.0.jar b Navdy-post-mortem-1
# apksigner.bat sign --ks ..\navdy_alelec.jks --out ..\apk\Navdy-post-mortem-1-Anon.apk Navdy-post-mortem-1\dist\Navdy-post-mortem-1.apk

import sys
import os, re
import shutil
import fileinput
from pathlib import Path

import xml.etree.ElementTree as ET

cd = Path(__file__).parent
merged = cd/'..'/'build'/'intermediates'/'incremental'/'mergeDebugResources'/'merged.dir'/'values'/'values.xml'

root = ET.parse(merged).getroot()

attribs = []
styleables = []

for child in root:
    if child.tag == 'declare-styleable':
        styleables.append(child)
        for attrs in child:
            try:
                name = attrs.attrib['name']
                if attrs.tag == 'attr' and not name.startswith('android:'):
                    print(attrs.tag, name)
                    attribs.append(name)
            except KeyError:
                pass



# public = cd/"Navdy-post-mortem-1"/"res"/"values"/"public.xml"
# et = ET.parse(public)
# root = et.getroot()

# for child in root:
#     try:
#         if child.attrib['name'] in attribs and child.attrib['type'] == "attr":
#             child.attrib['name'] = child.attrib['name']+"_orig"

#         # This one throws an error
#         if child.attrib['name'] == "actionModeStyle":
#             child.attrib['name'] = child.attrib['name']+"_orig"

#     except KeyError:
#         pass

# et.write(public)

attrs = cd/"de.volkswagen.mediacontrol"/"res"/"values"/"attrs.xml"

et = ET.parse(attrs)
root = et.getroot()


for child in root.findall('./'):
    try:
        if child.attrib['name'] in attribs:
            # child.attrib['name'] = child.attrib['name']+"__orig"
            root.remove(child)

    except KeyError:
        pass

# for s in styleables:
#     root.append(s)

et.write(attrs)


# for styles in (cd/"Navdy-post-mortem-1"/"res").glob("values*/styles.xml"):

#     body = styles.read_text()

#     # print('behavior_skipCollapsed' in attribs)

#     for each in attribs:
#         # print(each)
#         body = body.replace('"' + each + '"', '"' + each + '_orig"')

#     styles.write_text(body)
